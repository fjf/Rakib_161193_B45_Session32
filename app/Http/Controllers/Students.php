<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class Students extends Controller
{
    public function index() {

        $allStudents = [
            "0"=> [
                "name" => "Mr. X",
                "roll" => "SEIP: 112233",
                "result" => "Pass"
            ],
            "1"=> [
                "name" => "Mr. Y",
                "roll" => "SEIP: 445566",
                "result" => "Fail"
            ],
            "2"=> [
                "name" => "Mr. Z",
                "roll" => "SEIP: 778899",
                "result" => "Pass"
            ],

        ];

        return view('Students.index',compact('allStudents'));
    }





    public function store(){

        $obj = new Student();
        $obj->name = $_POST["name"];
        $obj->roll = $_POST["roll"];
        $obj->result = $_POST["result"];

        $result = $obj->save();

        if($result) echo "Success!";
        else echo "Failed";
    }



















}
