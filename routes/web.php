<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Students@index');



Route::get('/students/create', function(){

    return view("students.create");

});


Route::post('/students/store', 'Students@store');

Auth::routes();

Route::get('/home', 'HomeController@index');
